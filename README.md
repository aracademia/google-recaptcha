# Google reCaptcha
Aracademia Recaptcha is a light weight package for adding the new google reCaptcha to any form in your Laravel 5 application.
## Usage
###Step 1: Install Through Composer
Open composer.json file and add "aracademia/recaptcha": "1.*"
```
#!php

"require": {
        "aracademia/recaptcha": "1.*"
     }
```
Run *composer update* in your terminal
### Step 2: Add Service Provider ###
Navigate to config/app.php and *Aracademia\Recaptcha\RecaptchaServiceProvider::class,* under your providers

```
#!php

'providers' => [
    Aracademia\Recaptcha\RecaptchaServiceProvider::class,
]
```
### Step 3: Publish Config File ###
Run the following command in your terminal to publish the package's config file to your Laravel project's config folder

```
#!php

php artisan vendor:publish
```
### Step 4: Get reCAPTCHA Keys From Google ###
* Sign in to your google account
* Browse to the following link: https://www.google.com/recaptcha/intro/index.html
* Click get reCAPTCHA
* Add a Label and your Domain name then click Register
* Copy the public key and private key
* Go back to your Laravel project and open your .env file
* Paste the following in your .env file

```
#!php

RECAPTCHA_PUBLIC_KEY=Your_Public_key_goes_here
RECAPTCHA_PRIVATE_KEY=Your_private_key_goes_here
```
### Step 5: Display The reCaptcha Field In Your Forms ###
Open your form view where you want to display recaptcha field and paste the following code

```
#!php

{!! Recaptcha::inputField() !!}
```
### Step 6: Validate reCaptcha ###
Navigate to your controller where you are validating your form input field and add the following rule to your validation

```
#!php

public function store(Request $request)
    {
        $this->validate($request,
            [
                .......
                'g-recaptcha-response'  =>  'recaptcha'
            ]);

        //create user or send email....
    }
```