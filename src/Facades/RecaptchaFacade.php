<?php
/**
 * User: rrafia
 * Date: 12/17/15
 */

namespace Aracademia\Recaptcha\Facades;


use Illuminate\Support\Facades\Facade;

class RecaptchaFacade extends Facade {

    protected static function getFacadeAccessor()
    {
        return 'Recaptcha';
    }

} 