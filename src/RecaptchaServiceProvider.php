<?php
/**
 * User: rrafia
 * Date: 12/16/15
 */

namespace Aracademia\Recaptcha;

use Illuminate\Support\ServiceProvider;
use Illuminate\Foundation\AliasLoader;

class RecaptchaServiceProvider extends ServiceProvider {


    //Extends Validator to include a recaptcha type
    public function addValidator()
    {
        $this->app->validator->extendImplicit('recaptcha', function ($attribute, $value, $parameters) {
            $captcha   = new Recaptcha();
            return $captcha->validate();
        }, config('Recaptcha.custom_error'));
    }

    public function boot()
    {
        //Define path to the views folder
        $this->loadViewsFrom(__DIR__.'/Views','recaptcha');

        //Add Validator
        $this->addValidator();

        $this->publishes([
            __DIR__.'/config/RecaptchaConfig.php' => config_path('Recaptcha.php'),
        ]);
    }

    public function register()
    {
        $this->app['Recaptcha'] = $this->app->share(function($app)
        {
            return new Recaptcha();
        });

        //register our facades
        $this->app->booting(function()
        {
            AliasLoader::getInstance()->alias('Recaptcha','Aracademia\Recaptcha\Facades\RecaptchaFacade');
        });


    }

} 